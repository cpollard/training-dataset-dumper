#!/usr/bin/env bash

# Input derivation I got from rachael
INPUT_FILE=~/dust/toptag/mc16_13TeV/DAOD_FTAG5.15578469._000002.pool.root.1

if [[ ! -f $INPUT_FILE ]] ; then
    echo "ERROR: input file $INPUT_FILE not found" >&2
    exit 1
fi

# run the script
$AnalysisBase_DIR/bin/dump-single-btag $INPUT_FILE
