from sys import argv, stdout

from keras.models import Model
from keras.layers import Dense, Activation, Input, Add
from keras.callbacks import ProgbarLogger, ReduceLROnPlateau, TensorBoard
from keras.layers.normalization import BatchNormalization
from keras.initializers import RandomNormal
from keras.optimizers import Adam

import uproot

from scipy import stats
import numpy
import h5py

from math import isnan

escale = 1e6

ljobs = ["pt", "eta", "phi", "m"]
sjobs = \
    [ "pt", "eta", "phi", "m"
    , "DL1rnn_pu", "DL1rnn_pc", "DL1rnn_pb" \

    # , "JetFitter_energyFraction", "JetFitter_mass" \
    # , "JetFitter_significance3d", "JetFitter_deltaphi" \
    # , "JetFitter_deltaeta", "JetFitter_massUncorr" \
    # , "JetFitter_dRFlightDir" \
    # , "JetFitter_nVTX", "JetFitter_nSingleTracks" \
    # , "JetFitter_nTracksAtVtx", "JetFitter_N2Tpair" \
  
    # , "SV1_masssvx", "SV1_efracsvx", "SV1_significance3d" \
    # , "SV1_dstToMatLay", "SV1_deltaR", "SV1_Lxy", "SV1_L3d" \
    # , "SV1_N2Tpair", "SV1_NGTinSvx" \
  
    # , "rnnip_pu", "rnnip_pc", "rnnip_ptau", "rnnip_pb" \
    ]

ljlab = "largejet_label"

obs = \
        list(map(lambda s: "largejet_" + s, ljobs)) + \
        list(map(lambda s: "subjet0_" + s, sjobs)) + \
        list(map(lambda s: "subjet1_" + s, sjobs)) + \
        list(map(lambda s: "subjet2_" + s, sjobs))

ninputs = len(obs)

maxevents = int(2e5)

nljobs = len(ljobs)
nsjobs = len(sjobs)

def model():
    ljinputs = Input((nljobs,))
    sj1inputs = Input((nsjobs,))
    sj2inputs = Input((nsjobs,))
    sj3inputs = Input((nsjobs,))

    def layer(ins, n):
        last = Dense(activation="linear", units=n)(ins)
        # last = BatchNormalization()(last)
        last = Activation('relu')(last)
        return last


    def net(ins, nneurs):
        last = ins
        for n in nneurs:
            last = layer(last, n)

        return last

    sj1net = net(sj1inputs, [8, 6, 4])
    sj2net = net(sj2inputs, [8, 6, 4])
    sj3net = net(sj3inputs, [8, 6, 4])

    sj12net = net(Add()([sj1net, sj2net]), [8, 6, 4])
    sj23net = net(Add()([sj2net, sj3net]), [8, 6, 4])
    sj31net = net(Add()([sj3net, sj1net]), [8, 6, 4])

    totalinputs = \
            Add()([ljinputs, sj1net, sj2net, sj3net, sj12net, sj23net,
              sj31net])

    net = net(totalinputs, [40, 20, 10])

    final = Dense(activation="sigmoid", units=1)(net)

    return Model([ljinputs, sj1inputs, sj2inputs, sj3inputs], outputs=final)


def buildsamp(fname, maxevts=-1):
    f = uproot.open(fname)

    print("setting up file %s" % fname)
    stdout.flush()

    t = f["toptagger"]
    trw = f["reweight"]

    arrs = t.arrays(obs, entrystop=maxevts, outputtype=list)

    xs = numpy.stack(arrs).T

    # normalize all energies to a smaller scale
    xs[:,0] /= escale
    xs[:,3] /= escale
    for i in [0, 1, 2]:
        idx = 4+i*len(sjobs)
        xs[:,idx] /= escale
        xs[:,idx+3] /= escale

    ys = t.array(ljlab, entrystop=maxevts)

    ws = t.array("weight", entrystop=maxevts)
    rws = trw.array("reweight", entrystop=maxevts)

    return xs, ys, ws*rws


def shuffle_coherently(xs):
    rng_state = numpy.random.get_state()
    for x in xs:
        numpy.random.set_state(rng_state)
        numpy.random.shuffle(x)

    return


def wgt_cut(wgts):
    m = numpy.mean(wgts)
    s = numpy.std(wgts)

    return wgts < m + s

def main(outprefix, sigfnames, bkgfnames):

    m = model()

    print("signal files:")
    list(map(print, sigfnames))
    print("")

    print("background files:")
    list(map(print, bkgfnames))
    print("")


    # TODO
    # so much boiler......
    bkgxs = []
    bkgys = []
    bkgws = []
    for bkgfname in bkgfnames:
        (bkgx, bkgy, bkgw) = buildsamp(bkgfname, maxevents)
        bkgxs.append(bkgx)
        bkgys.append(bkgy)
        bkgws.append(bkgw)

    bkgx = numpy.concatenate(bkgxs)
    bkgy = numpy.concatenate(bkgys)
    bkgw = numpy.concatenate(bkgws)
    bkgw /= numpy.sum(bkgw)

    # cut = wgt_cut(bkgw)
    # bkgx = bkgx[cut]
    # bkgy = bkgy[cut]
    # bkgw = bkgw[cut]

    shuffle_coherently([bkgx, bkgy, bkgw])


    sigxs = []
    sigys = []
    sigws = []
    for sigfname in sigfnames:
        (sigx, sigy, sigw) = buildsamp(sigfname, maxevents)
        sigxs.append(sigx)
        sigys.append(sigy)
        sigws.append(sigw)


    # remove non-signal jets
    sigytmp = numpy.concatenate(sigys)
    sigcut = sigytmp > 0.5

    sigx = numpy.concatenate(sigxs)[sigcut]
    sigy = sigytmp[sigcut]
    sigw = numpy.concatenate(sigws)[sigcut]
    sigw /= numpy.sum(sigw)

    # cut = wgt_cut(sigw)
    # sigx = sigx[cut]
    # sigy = sigy[cut]
    # sigw = sigw[cut]

    shuffle_coherently([sigx, sigy, sigw])


    print("number of signal events: %d" % len(sigx))
    print("average signal weight: %.2e" % numpy.mean(sigw))

    print("number of background events: %d" % len(bkgx))
    print("average background weight: %.2e" % numpy.mean(bkgw))

    numpy.save(outprefix + ".sigdists.npy", sigx)
    numpy.save(outprefix + ".bkgdists.npy", bkgx)
    numpy.save(outprefix + ".sigwgts.npy", sigw)
    numpy.save(outprefix + ".bkgwgts.npy", bkgw)

    xs = numpy.concatenate([sigx, bkgx])
    ys = numpy.concatenate([sigy, bkgy])
    ws = numpy.concatenate([sigw, bkgw])

    shuffle_coherently([xs, ys, ws])

    # ys = numpy.stack([ys, 1-ys]).T


    if numpy.any(numpy.isnan(xs)):
        print("NAAAAAAN!")


    model_optimizer = Adam(lr=5e-3)

    m.compile(loss='binary_crossentropy',
            optimizer=model_optimizer, metrics=["accuracy"])

    callbacks = \
            [ TensorBoard(update_freq=10000)
            # , ProgbarLogger()
            , ReduceLROnPlateau()
            ]

    print("average signal target: %.2e" % numpy.mean(sigy))
    print("average background target: %.2e" %  numpy.mean(bkgy))

    def splitins(ins):
        sjins = []
        for i in [0, 1, 2]:
            idx = 4+i*nsjobs
            sjins.append(ins[:,idx:idx+nsjobs])

        return [ins[:,0:4]] + sjins


    sigins = splitins(sigx)
    bkgins = splitins(bkgx)


    print("average signal prediction: %.2e" %
        numpy.mean(m.predict(sigins), axis=0))
    print("average background prediction: %.2e" %
        numpy.mean(m.predict(bkgins), axis=0))


    history = \
          m.fit(splitins(xs), ys, sample_weight=ws, epochs=5, verbose=2,
                    validation_split=0.10, batch_size=200,
                    callbacks=callbacks)


    sigpred = m.predict(sigins)
    bkgpred = m.predict(bkgins)


    outf = open(outprefix + ".json", 'w')
    outf.write(m.to_json())
    outf.close()

    m.save_weights(outprefix + ".h5")

    numpy.save(outprefix + ".sigpred.npy", sigpred)
    numpy.save(outprefix + ".bkgpred.npy", bkgpred)


def cleannan(x):
    y = float(x)
    return (0 if isnan(y) else y)


if __name__ == "__main__":
    bkgs = []
    sigs = []
    l = None
    for arg in argv[2:]:
        if arg == "-s":
            l = sigs
        elif arg == "-b":
            l = bkgs
        else:
            l.append(arg)

    main(argv[1], sigs, bkgs)
