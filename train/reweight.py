from sys import argv, stdout
import ROOT
import array

dijetdict = \
        { 361020 : (78420000000.0 , 0.9755) \
        , 361021 : (78420000000.0 , 0.000672) \
        , 361022 : (2433200000.0  , 0.000334) \
        , 361023 : (26454000.0    , 0.00032) \
        , 361024 : (254630.0      , 0.000531) \
        , 361025 : (4553.5        , 0.000924) \
        , 361026 : (257.53        , 0.000943) \
        , 361027 : (16.215        , 0.000393) \
        , 361028 : (0.62504       , 0.010176) \
        , 361029 : (0.019639      , 0.012076) \
        , 361030 : (0.0011962     , 0.005909) \
        , 361031 : (4.2259e-05    , 0.002676) \
        , 361032 : (1.0367e-06    , 0.000426) \
        }


def getSampInfo(fin):
    tw = fin.Get("weights")

    if tw.GetEntries() == 0:
        return (-1, 0)

    dsid = array.array('i', [0])
    weight = array.array('f', [0])

    tw.SetBranchAddress("dsid", dsid)
    tw.SetBranchAddress("sumOfWeights", weight)


    sumw = 0
    for i in xrange(tw.GetEntries()):
        tw.GetEntry(i)
        sumw += weight[0]
        continue

    print("dsid: %d" % dsid[0])
    print("dijet sample" if dsid[0] in dijetdict else "signal sample")
    stdout.flush()

    return (dsid[0], sumw)


def getHist(fin, dsid):
  if dsid in dijetdict:
    return fin.Get("hpteta_bkg")
  else:
    return fin.Get("hpteta_sig")


def main(fnames):

    fins = []
    sampdict = {}
    for fname in fnames:
        print("reading info from file %s" % fname)
        stdout.flush()

        fin = ROOT.TFile.Open(fname, "UPDATE")
        fins.append(fin)

        (dsid, sumw) = getSampInfo(fin)

        if dsid in sampdict:
            (sumw1, h1) = sampdict[dsid]
            h = getHist(fin, dsid)
            h1.Add(h)
            sampdict[dsid] = (sumw+sumw1, h1)
        else:
            h = getHist(fin, dsid)
            sampdict[dsid] = (sumw, h.Clone(h.GetName() + "_total"))

        continue


    sighist = None
    bkghist = None
    for (dsid, (sumw, h)) in sampdict.iteritems():
        (xsec, filteff) = dijetdict.get(dsid, (1.0, 1.0))

        h1 = h.Clone(h.GetName() + "weighted")
        h1.Scale(xsec*filteff/sumw)

        if dsid in dijetdict:
            if bkghist is None:
                bkghist = h1.Clone("bkghist")
            else:
                bkghist.Add(h1)

        else:
            if sighist is None:
                sighist = h1.Clone("sighist")
            else:
                sighist.Add(h1)

        continue

    bkgpthist = bkghist.ProjectionX()
    sigpthist = sighist.ProjectionX()
    ratpthist = bkgpthist.Clone("ratio")
    ratpthist.Divide(sigpthist)


    dsid = array.array('i', [0])
    pt = array.array('f', [0])

    # ptsj1 = array.array('f', [0])
    # etasj1 = array.array('f', [0])
    # phisj1 = array.array('f', [0])
    # msj1 = array.array('f', [0])

    # ptsj2 = array.array('f', [0])
    # etasj2 = array.array('f', [0])
    # phisj2 = array.array('f', [0])
    # msj2 = array.array('f', [0])

    # ptsj3 = array.array('f', [0])
    # etasj3 = array.array('f', [0])
    # phisj3 = array.array('f', [0])
    # msj3 = array.array('f', [0])

    reweight = array.array('f', [0])
    # m01 = array.array('f', [0])
    # m12 = array.array('f', [0])
    # m20 = array.array('f', [0])

    for fin in fins:
        print ("")
        print("writing reweight to file %s" % fin.GetName())
        stdout.flush()

        fin.cd()

        print("checking for already-existing reweight tree.")
        stdout.flush()

        if fin.GetListOfKeys().Contains("reweight"):
          print("already had reweight tree; deleting.")
          stdout.flush()
          fin.Delete("reweight")

        print("creating reweight TTree.")
        stdout.flush()

        trw = ROOT.TTree("reweight", "reweight")

        reweight[0] = 0.0
        print("creating reweight TBranch.")
        stdout.flush()

        trw.Branch("reweight", reweight, "reweight/F")

        print("created reweight TBranch.")
        stdout.flush()


        # trw.Branch("m01", m01, "m01/F")
        # trw.Branch("m12", m12, "m12/F")
        # trw.Branch("m20", m20, "m20/F")

        for k in fin.GetListOfKeys():
          print(k.GetName())
          stdout.flush()

        print("getting toptagger tree.")
        stdout.flush()
        t = fin.Get("toptagger")

        print("toptagger tree:")
        print(t)
        stdout.flush()

        t.SetBranchAddress("largejet_pt", pt)

        # t.SetBranchAddress("subjet0_pt", ptsj1)
        # t.SetBranchAddress("subjet0_eta", etasj1)
        # t.SetBranchAddress("subjet0_phi", phisj1)
        # t.SetBranchAddress("subjet0_m", msj1)

        # t.SetBranchAddress("subjet1_pt", ptsj2)
        # t.SetBranchAddress("subjet1_eta", etasj2)
        # t.SetBranchAddress("subjet1_phi", phisj2)
        # t.SetBranchAddress("subjet1_m", msj2)

        # t.SetBranchAddress("subjet2_pt", ptsj3)
        # t.SetBranchAddress("subjet2_eta", etasj3)
        # t.SetBranchAddress("subjet2_phi", phisj3)
        # t.SetBranchAddress("subjet2_m", msj3)

        print("making a friend :-)")
        stdout.flush()
        trw.AddFriend(t)

        print("reading in dsid and weights")
        stdout.flush()
        tw = fin.Get("weights")
        tw.SetBranchAddress("dsid", dsid)
        tw.GetEntry(0)

        (sumw, blah) = sampdict[dsid[0]]
        (xsec, filteff) = dijetdict.get(dsid[0], (1.0, 1.0))

        sampwgt = xsec*filteff/sumw

        isSig = dsid[0] not in dijetdict

        print("about to loop over entries for reweighting.")
        stdout.flush()
        for i in xrange(t.GetEntries()):
            t.GetEntry(i)

            # tlvsj1 = ROOT.TLorentzVector()
            # tlvsj1.SetPtEtaPhiM(ptsj1[0], etasj1[0], phisj1[0],
            #     msj1[0])
            # tlvsj2 = ROOT.TLorentzVector()
            # tlvsj2.SetPtEtaPhiM(ptsj2[0], etasj2[0], phisj2[0],
            #     msj2[0])
            # tlvsj3 = ROOT.TLorentzVector()
            # tlvsj3.SetPtEtaPhiM(ptsj3[0], etasj3[0], phisj3[0],
            #     msj3[0])

            # m01[0] = (tlvsj1 + tlvsj2).M()
            # m12[0] = (tlvsj2 + tlvsj3).M()
            # m20[0] = (tlvsj3 + tlvsj1).M()


            if isSig:
                reweight[0] = \
                    sampwgt \
                    * ratpthist.GetBinContent(ratpthist.FindBin(pt[0]))

            else:
                reweight[0] = sampwgt


            trw.Fill()
            continue

        trw.Write("reweight", ROOT.TFile.kOverwrite)
        fin.Close()
        continue

    print fins
    return 0


if __name__ == "__main__":
    main(argv[1:])
