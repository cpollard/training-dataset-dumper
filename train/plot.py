import matplotlib.pyplot as plt

import numpy

from sys import argv

sigpreds = numpy.load(argv[2])
sigwgts = numpy.load(argv[3])

bkgpreds = numpy.load(argv[5])
bkgwgts = numpy.load(argv[6])

sigdists = numpy.load(argv[1])
bkgdists = numpy.load(argv[4])
for i in range(sigdists.shape[1]):
    plt.clf()

    (blah, bins, blah1) = \
            plt.hist(sigdists[:,i], weights=sigwgts, density=True,
                    alpha=0.5, bins=50)
    plt.hist(bkgdists[:,i], weights=bkgwgts, density=True, alpha=0.5,
            bins=bins)

    plt.yscale('log', nonposy='clip')
    plt.legend(["signal", "background"])
    plt.savefig("%03d.dist.png" % i)


plt.clf()

plt.hist(sigpreds, weights=sigwgts, alpha=0.5, bins=50,
        range=(0, 1))
plt.hist(bkgpreds, weights=bkgwgts, alpha=0.5, bins=50,
        range=(0, 1))

plt.legend(["signal", "background"])

plt.savefig("preds.png")


plt.clf()

(hsig, blah) = numpy.histogram(sigpreds[:,0], weights=sigwgts, bins=50,
        range=(0, 1))
(hbkg, blah) = numpy.histogram(bkgpreds[:,0], weights=bkgwgts, bins=50,
        range=(0, 1))

sigeff = numpy.cumsum(hsig[::-1])
bkgeff = numpy.cumsum(hbkg[::-1])


plt.semilogy(sigeff, 1/bkgeff)

plt.savefig("roc.png")
