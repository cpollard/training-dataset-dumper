#ifndef HBB_CONFIG_HH
#define HBB_CONFIG_HH

#include <string>
#include <vector>

struct SubjetConfig {
  std::string input_name;
  std::string output_name;
  bool store_tracks;
  bool get_subjets_from_parent;
  size_t n_subjets_to_save;
};



struct HbbConfig {
  std::vector<SubjetConfig> subjet_configs;
  std::string jet_collection;
  std::string jet_calib_file;
  std::string cal_seq;
  std::string cal_area;
};

HbbConfig get_hbb_config(const std::string& config_file_name);

#endif
