#include <TFile.h>
#include <TTree.h>
#include <TH1.h>
#include <TH2.h>
#include <map>
#include <iostream>


using namespace std;

typedef TFile* fptr;
typedef TH2F* h2ptr;
typedef TH1D* hptr;
typedef TTree* tptr;

pair<int, float> getSampInfo(TFile* f) {
  tptr tw = dynamic_cast<tptr>(f->Get("weights"));

  if (tw->GetEntries() == 0)
    return make_pair(-1, 0);

  int dsidtmp = 0;
  float weight = 0;

  tw->SetBranchAddress("dsid", &dsidtmp);
  tw->SetBranchAddress("sumOfWeights", &weight);


  float sumw = 0;
  for (int i = 0; i < tw->GetEntries(); i++) {
    tw->GetEntry(i);
    sumw += weight;
  }

  int dsid = dsidtmp;

  return make_pair(dsid, sumw);
}


const h2ptr getHist(TFile* fin, bool isbkg) {
  if (isbkg)
    return dynamic_cast<h2ptr>(fin->Get("hpteta_bkg"));
  else
    return dynamic_cast<h2ptr>(fin->Get("hpteta_sig"));
}


template <class K, class T>
bool inmap(const K& k, const map<K, T>& m) {
  return !(m.find(k) == m.end());
}


int main(int argc, char* argv[]) {

  map<int, float> dijetdict;
  dijetdict[361020] = 78420000000.0 * 0.9755;
  dijetdict[361021] = 78420000000.0 * 0.000672;
  dijetdict[361022] = 2433200000.0  * 0.000334;
  dijetdict[361023] = 26454000.0    * 0.00032;
  dijetdict[361024] = 254630.0      * 0.000531;
  dijetdict[361025] = 4553.5        * 0.000924;
  dijetdict[361026] = 257.53        * 0.000943;
  dijetdict[361027] = 16.215        * 0.000393;
  dijetdict[361028] = 0.62504       * 0.010176;
  dijetdict[361029] = 0.019639      * 0.012076;
  dijetdict[361030] = 0.0011962     * 0.005909;
  dijetdict[361031] = 4.2259e-05    * 0.002676;
  dijetdict[361032] = 1.0367e-06    * 0.000426;


  map<int, pair<float, h2ptr> > sampdict;
  vector<fptr> fins;
  for (int i = 1; i < argc; i++) {
    const char* fname = argv[i];
    fptr fin = TFile::Open(fname, "UPDATE");

    fins.push_back(fin);

    pair<int, float> dsidsumw = getSampInfo(fin);
    int dsid = dsidsumw.first;
    float sumw = dsidsumw.second;

    bool isbkg = inmap(dsid, dijetdict);
    const h2ptr h = getHist(fin, isbkg);

    if (inmap(dsid, sampdict)) {
      auto sumwh1 = sampdict[dsid];
      float sumw1 = sumwh1.first;
      h2ptr h1 = sumwh1.second;
      h1->Add(h);

      sampdict[dsid] = make_pair(sumw+sumw1, h1);
    } else {
      sampdict[dsid] =
        make_pair(sumw, dynamic_cast<h2ptr>(h->Clone()));
    }
  }


  hptr sighist = NULL;
  hptr bkghist = NULL;

  for (const auto &p : sampdict) {
    int dsid = p.first;
    float sumw = p.second.first;
    h2ptr h = p.second.second;

    float xsxfe;
    bool isbkg;
    if (inmap(dsid, dijetdict)) {
      xsxfe = dijetdict[dsid];
      isbkg = true;
    } else {
      xsxfe = 1.0;
      isbkg = false;
    }

    cout << (isbkg ? "background" : "signal")
      << " process with dsid " << dsid << endl
      << "has cross section x filt eff "
      << xsxfe << endl << "and sumw "
      << sumw << endl << endl;

    cout << "histogram integral before scaling: "
      << h->Integral() << endl << endl;

    h2ptr h1 = dynamic_cast<h2ptr>(h->Clone());
    h1->Scale(xsxfe/sumw);

    cout << "histogram integral after scaling: "
      << h1->Integral() << endl << endl;

    if (isbkg) {
      if (bkghist)
        bkghist->Add(h1->ProjectionX());
      else
        bkghist = dynamic_cast<TH1D*>(h1->ProjectionX()->Clone());

      cout << "new background integral: "
        << bkghist->Integral() << endl << endl;
    } else {
      if (sighist)
        sighist->Add(h1->ProjectionX());
      else
        sighist = dynamic_cast<TH1D*>(h1->ProjectionX()->Clone());

      cout << "new signal integral: "
        << sighist->Integral() << endl << endl;
    }
  }

  cout << "background histogram total integral: " << bkghist->Integral()
    << endl << endl;

  cout << "signal histogram total integral: " << sighist->Integral()
    << endl << endl;

  hptr ratiohist = dynamic_cast<hptr>(bkghist->Clone("ratio"));
  ratiohist->Divide(sighist);

  float pt, reweight;
  for (TFile* fin : fins) {
    cout << "reading in file " << fin->GetName() << endl;

    if (fin->GetListOfKeys()->Contains("reweight")) {
      cout << "already had reweight tree; deleting." << endl;
      fin->Delete("reweight");
    }

    fin->cd();

    tptr trw = new TTree("reweight", "reweight");
    trw->Branch("reweight", &reweight, "reweight/F");

    tptr t = dynamic_cast<tptr>(fin->Get("toptagger"));
    t->SetBranchAddress("largejet_pt", &pt);
    trw->AddFriend(t);

    int dsid = getSampInfo(fin).first;

    float sumw = sampdict[dsid].first;

    float xsxfe;
    bool isbkg;
    if (inmap(dsid, dijetdict)) {
      xsxfe = dijetdict[dsid];
      isbkg = true;
    } else {
      xsxfe = 1.0;
      isbkg = false;
    }

    float sampwgt = xsxfe/sumw;
    cout << "sample weight: " << sampwgt << endl;

    for (int i = 0; i < t->GetEntries(); i++) {
      t->GetEntry(i);

      if (isbkg)
        reweight = sampwgt;
      else
        reweight =
          sampwgt*ratiohist->GetBinContent(ratiohist->FindBin(pt));

      trw->Fill();
    }

    trw->Write("reweight", TFile::kOverwrite);
    bkghist->Write("hbkg", TFile::kOverwrite);
    sighist->Write("hsig", TFile::kOverwrite);
    ratiohist->Write("hratio", TFile::kOverwrite);
    fin->Close();
  }

  return 0;
}
