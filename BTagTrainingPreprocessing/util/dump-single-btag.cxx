#include <cstddef>
#include <memory>
#include <cmath>

#include "BTaggingWriterConfiguration.hh"
#include "BTagJetWriterConfig.hh"
#include "BTagTrackWriterConfig.hh"
#include "SingleBTagOptions.hh"
#include "BTagJetWriter.hh"
#include "BTagTrackWriter.hh"
#include "TrackSelector.hh"

#include "FlavorTagDiscriminants/BTagJetAugmenter.h"
#include "FlavorTagDiscriminants/BTagTrackAugmenter.h"

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

#include "JetCalibTools/JetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

#include "xAODRootAccess/TEvent.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODTracking/TrackParticleContainer.h"

#include "H5Cpp.h"

#include "TFile.h"
#include "TTree.h"

namespace {
  bool is_from_W_or_Z(const xAOD::TruthParticle &truth_particle) {
    for (std::size_t parent_index = 0; parent_index < truth_particle.nParents(); parent_index++) {
      const xAOD::TruthParticle &parent = *truth_particle.parent(parent_index);
      if (parent.isW() || parent.isZ() || is_from_W_or_Z(parent)) return true;
    }
    return false;
  }

  bool is_overlaping_electron_or_muon(const xAOD::Jet &jet, const xAOD::TruthParticleContainer &truth_particles) {
    for (const xAOD::TruthParticle* truth_particle: truth_particles) {

      // protection for slimmed event record
      if ( ! truth_particle) continue;

      // now check if it's an overlaping election or muon
      if (! (truth_particle->isElectron() || truth_particle->isMuon())) continue;
      if (truth_particle->pt() < 10000 || truth_particle->status() != 1) continue;
      if (jet.p4().DeltaR(truth_particle->p4()) > 0.3) continue;

      // ...from a W or Z
      if (is_from_W_or_Z(*truth_particle)) return true;
    } // end of particle loop
    return false;
  }

  // sort functions
  bool by_d0(const xAOD::TrackParticle* t1,
             const xAOD::TrackParticle* t2) {
    static SG::AuxElement::ConstAccessor<float> d0("btag_ip_d0");
    return std::abs(d0(*t1)) > std::abs(d0(*t2));
  }

}

int main (int argc, char *argv[]) {
  SingleTagIOOpts opts = get_single_tag_io_opts(argc, argv);
  // The name of the application:
  const char *const APP_NAME = "BTagTestDumper";

  // Set up the environment:
  RETURN_CHECK( APP_NAME, xAOD::Init() );

  // Set up the event object:
  xAOD::TEvent event(xAOD::TEvent::kClassAccess);

  // Initialize JetCalibrationTool with release 21 recommendations
  JetCalibrationTool calib_tool("JetCalibrationTool");
  RETURN_CHECK( APP_NAME, calib_tool.setProperty("JetCollection", "AntiKt4EMTopo") );
  RETURN_CHECK( APP_NAME, calib_tool.setProperty("ConfigFile", "JES_data2017_2016_2015_Recommendation_Feb2018_rel21.config") );
  RETURN_CHECK( APP_NAME, calib_tool.setProperty("CalibSequence", "JetArea_Residual_EtaJES_GSC") );
  RETURN_CHECK( APP_NAME, calib_tool.setProperty("CalibArea", "00-04-81") );
  RETURN_CHECK( APP_NAME, calib_tool.setProperty("IsData", false) );
  RETURN_CHECK( APP_NAME, calib_tool.initialize() );

  JetCleaningTool jetcleaningtool("JetCleaningTool", JetCleaningTool::LooseBad, false);
  RETURN_CHECK( APP_NAME, jetcleaningtool.initialize() );

  JetVertexTaggerTool jvttool("JetVertexTaggerTool");
  RETURN_CHECK( APP_NAME, jvttool.initialize() );

  // this tool adds variables we need for b-tagging which are derived
  // from the jet
  TrackSelector track_selector;
  BTagJetAugmenter jet_augmenter;
  BTagTrackAugmenter track_augmenter;

  // new way to do output files
  H5::H5File output(opts.out, H5F_ACC_TRUNC);
  // set up jet writer
  BTagJetWriterConfig jet_cfg;
  jet_cfg.write_event_info = true;
  cfg::insert_into(jet_cfg.char_variables, cfg::BTagAddedChars);
  cfg::insert_into(jet_cfg.truth_labels, cfg::BTagTruthLabels);
  cfg::insert_into(jet_cfg.jet_float_variables, cfg::BTagTruthFloats);
  cfg::insert_into(jet_cfg.int_as_float_variables, cfg::BTagInts);
  cfg::insert_into(jet_cfg.int_as_float_variables, cfg::BTagAddedInts);
  cfg::insert_into(jet_cfg.float_variables, cfg::BTagFloats);
  cfg::insert_into(jet_cfg.float_variables, cfg::BTagAddedFloats);
  cfg::insert_into(jet_cfg.float_variables, cfg::SMTFloats);
  cfg::insert_into(jet_cfg.double_variables, cfg::BTagDoubles);
  cfg::insert_into(jet_cfg.double_variables, cfg::BTagAddedDoubles);
  cfg::insert_into(jet_cfg.double_variables, cfg::SMTDoubles);
  jet_cfg.variable_maps.replace_with_defaults_checks = cfg::check_map_from(cfg::BTagDefaultsMap);
  jet_cfg.variable_maps.rename = {}; // please don't use this :(
  jet_cfg.name = "jets";
  BTagJetWriter jet_writer(output, jet_cfg);
  // set up track writer
  BTagTrackWriterConfig track_cfg;
  track_cfg.name = "tracks";
  track_cfg.uchar_variables = cfg::TrackUChars;
  track_cfg.int_variables = cfg::TrackInts;
  track_cfg.float_variables = cfg::TrackFloats;
  track_cfg.output_size = {10};
  std::unique_ptr<BTagTrackWriter> track_writer(nullptr);
  if (opts.save_tracks) {
    track_writer.reset(new BTagTrackWriter(output, track_cfg));
  }

  // Loop over the specified files:
  for (const std::string& file: opts.in) {
    // Open the file:
    std::unique_ptr<TFile> ifile(TFile::Open(file.c_str(), "READ"));
    if ( ! ifile.get() || ifile->IsZombie()) {
      Error( APP_NAME, "Couldn't open file: %s", file.c_str() );
      return 1;
    }
    Info( APP_NAME, "Opened file: %s", file.c_str() );

    // Connect the event object to it:
    RETURN_CHECK( APP_NAME, event.readFrom(ifile.get()) );

    // Loop over its events:
    unsigned long long entries = event.getEntries();
    if (opts.max_events > 0) entries = std::min(opts.max_events, entries);
    for (unsigned long long entry = 0; entry < entries; ++entry) {

      // Load the event:
      if (event.getEntry(entry) < 0) {
        Error( APP_NAME, "Couldn't load entry %lld from file: %s",
               entry, file.c_str() );
        return 1;
      }

      // Print some status:
      if ( ! (entry % 500)) {
        Info( APP_NAME, "Processing entry %lld / %lld", entry, entries );
      }

      const xAOD::JetContainer *jets = nullptr;
      RETURN_CHECK( APP_NAME, event.retrieve(jets, "AntiKt4EMTopoJets") );

      const xAOD::TruthParticleContainer *truth_particles = nullptr;
      RETURN_CHECK( APP_NAME, event.retrieve(truth_particles, "TruthParticles") );

      const xAOD::EventInfo *event_info = nullptr;
      RETURN_CHECK( APP_NAME, event.retrieve(event_info, "EventInfo") );

      for (const xAOD::Jet *const uncalib_jet : *jets) {
        std::unique_ptr<const xAOD::Jet> calib_jet(nullptr);
        xAOD::Jet *i_love_you_asg(nullptr);
        calib_tool.calibratedCopy(*uncalib_jet, i_love_you_asg);
        calib_jet.reset(i_love_you_asg);

        if (is_overlaping_electron_or_muon(*calib_jet, *truth_particles)) {
          continue;
        }

        float updated_jvt_value= jvttool.updateJvt(*calib_jet);
        if (calib_jet->pt() > 20000 && calib_jet->pt() < 60000 && std::abs(calib_jet->eta()) < 2.4 && updated_jvt_value < 0.59) continue;

        if ( ! jetcleaningtool.keep(*calib_jet)) continue;

        if (calib_jet->pt() < 20000 || std::abs(calib_jet->eta()) > 2.5) {
          continue;
        }

        jet_augmenter.augment(*calib_jet, *uncalib_jet);
        jet_writer.write(*calib_jet, event_info);

        if (track_writer) {
          auto tracks = track_selector.get_tracks(*calib_jet);
          for (const auto& track: tracks) {
            track_augmenter.augment(*track, *calib_jet);
          }
          sort(tracks.begin(), tracks.end(), by_d0);
          track_writer->write(tracks, *calib_jet);
        }
      }
    }
  }
  return 0;
}
