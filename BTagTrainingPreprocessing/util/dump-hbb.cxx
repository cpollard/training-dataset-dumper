/// @file dump-test.cxx


/// @brief Test reading the main calo cluster and track particle container
///
/// This test simply reads in the static payload of the track particle
/// container of a primary xAOD. And checks how fast this can actually
/// be done.

#include "BookKeeper.hh"
#include "HbbOptions.hh"
#include "HbbConfig.hh"

// System include(s):
#include <memory>
#include <string>

// ROOT include(s):
#include <TFile.h>
#include <TH2.h>
#include <TError.h>
#include <TLorentzVector.h>

// AnalysisBase tool include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"
#include "AsgTools/AnaToolHandle.h"

// EDM include(s):
#include "JetCalibTools/JetCalibrationTool.h"
#include "xAODJet/JetContainer.h"


typedef std::vector<ElementLink<xAOD::IParticleContainer> > ParticleLinks;
SG::AuxElement::ConstAccessor<ParticleLinks> subjet_acc("ExKt3SubJets");

struct LargeJet {
  float pt, eta, phi, m, label;
};

void largejet(const xAOD::Jet* j, LargeJet& out) {
  out.pt = j->pt();
  out.eta = j->eta();
  out.phi = j->phi();
  out.m = j->m();


  typedef ElementLink<xAOD::JetContainer> JetLink;
  static AuxElement::ConstAccessor<JetLink> acc_parent("Parent");

  const xAOD::Jet* parent = *acc_parent(*j);

  static SG::AuxElement::ConstAccessor<int> GhostTQuarksFinalCount_acc("GhostTQuarksFinalCount");

  out.label = GhostTQuarksFinalCount_acc(*parent) > 0;

  return;
}

struct Subjet {
  float
    pt, eta, phi, m,

    MV2c10rnn_discriminant,

    DL1rnn_pu, DL1rnn_pc, DL1rnn_pb,

    rnnip_pu, rnnip_pc, rnnip_pb, rnnip_ptau,

    JetFitter_energyFraction,
    JetFitter_mass,
    JetFitter_significance3d,
    JetFitter_deltaphi,
    JetFitter_deltaeta,
    JetFitter_massUncorr,
    JetFitter_dRFlightDir,
    JetFitter_nVTX,
    JetFitter_nSingleTracks,
    JetFitter_nTracksAtVtx,
    JetFitter_N2Tpair,

    SV1_masssvx,
    SV1_efracsvx,
    SV1_significance3d,
    SV1_dstToMatLay,
    SV1_deltaR,
    SV1_Lxy,
    SV1_L3d,
    SV1_N2Tpair,
    SV1_NGTinSvx;
};


void subjet(const xAOD::Jet* j, size_t i, Subjet& out) {
  static SG::AuxElement::ConstAccessor<double> MV2c10rnn_discriminant_acc("MV2c10rnn_discriminant");

  static SG::AuxElement::ConstAccessor<double> DL1rnn_pu_acc("DL1rnn_pu");
  static SG::AuxElement::ConstAccessor<double> DL1rnn_pc_acc("DL1rnn_pc");
  static SG::AuxElement::ConstAccessor<double> DL1rnn_pb_acc("DL1rnn_pb");

  static SG::AuxElement::ConstAccessor<double> rnnip_pu_acc("rnnip_pu");
  static SG::AuxElement::ConstAccessor<double> rnnip_pc_acc("rnnip_pc");
  static SG::AuxElement::ConstAccessor<double> rnnip_pb_acc("rnnip_pb");
  static SG::AuxElement::ConstAccessor<double> rnnip_ptau_acc("rnnip_ptau");

  static SG::AuxElement::ConstAccessor<float> JetFitter_energyFraction_acc("JetFitter_energyFraction");
  static SG::AuxElement::ConstAccessor<float> JetFitter_mass_acc("JetFitter_mass");
  static SG::AuxElement::ConstAccessor<float> JetFitter_significance3d_acc("JetFitter_significance3d");
  static SG::AuxElement::ConstAccessor<float> JetFitter_deltaphi_acc("JetFitter_deltaphi");
  static SG::AuxElement::ConstAccessor<float> JetFitter_deltaeta_acc("JetFitter_deltaeta");
  static SG::AuxElement::ConstAccessor<float> JetFitter_massUncorr_acc("JetFitter_massUncorr");
  static SG::AuxElement::ConstAccessor<float> JetFitter_dRFlightDir_acc("JetFitter_dRFlightDir");
  static SG::AuxElement::ConstAccessor<int> JetFitter_nVTX_acc("JetFitter_nVTX");
  static SG::AuxElement::ConstAccessor<int> JetFitter_nSingleTracks_acc("JetFitter_nSingleTracks");
  static SG::AuxElement::ConstAccessor<int> JetFitter_nTracksAtVtx_acc("JetFitter_nTracksAtVtx");
  static SG::AuxElement::ConstAccessor<int> JetFitter_N2Tpair_acc("JetFitter_N2Tpair");

  static SG::AuxElement::ConstAccessor<float> SV1_masssvx_acc("SV1_masssvx");
  static SG::AuxElement::ConstAccessor<float> SV1_efracsvx_acc("SV1_efracsvx");
  static SG::AuxElement::ConstAccessor<float> SV1_significance3d_acc("SV1_significance3d");
  static SG::AuxElement::ConstAccessor<float> SV1_dstToMatLay_acc("SV1_dstToMatLay");
  static SG::AuxElement::ConstAccessor<float> SV1_deltaR_acc("SV1_deltaR");
  static SG::AuxElement::ConstAccessor<float> SV1_Lxy_acc("SV1_Lxy");
  static SG::AuxElement::ConstAccessor<float> SV1_L3d_acc("SV1_L3d");
  static SG::AuxElement::ConstAccessor<int> SV1_N2Tpair_acc("SV1_N2Tpair");
  static SG::AuxElement::ConstAccessor<int> SV1_NGTinSvx_acc("SV1_NGTinSvx");

  const ParticleLinks psj = subjet_acc(*j);
  const xAOD::Jet* sj = dynamic_cast<const xAOD::Jet*>(*(psj.at(i)));

  const xAOD::BTagging* btagging = sj->btagging();

  out.pt = sj->pt();
  out.eta = sj->eta();
  out.phi = sj->phi();
  out.m = sj->m();

  out.MV2c10rnn_discriminant = MV2c10rnn_discriminant_acc(*btagging);

  out.DL1rnn_pu = DL1rnn_pu_acc(*btagging);
  out.DL1rnn_pc = DL1rnn_pc_acc(*btagging);
  out.DL1rnn_pb = DL1rnn_pb_acc(*btagging);

  out.rnnip_pu = rnnip_pu_acc(*btagging);
  out.rnnip_pc = rnnip_pc_acc(*btagging);
  out.rnnip_pb = rnnip_pb_acc(*btagging);
  out.rnnip_ptau = rnnip_ptau_acc(*btagging);

  out.JetFitter_energyFraction = JetFitter_energyFraction_acc(*btagging);
  out.JetFitter_mass = JetFitter_mass_acc(*btagging);
  out.JetFitter_significance3d = JetFitter_significance3d_acc(*btagging);
  out.JetFitter_deltaphi = JetFitter_deltaphi_acc(*btagging);
  out.JetFitter_deltaeta = JetFitter_deltaeta_acc(*btagging);
  out.JetFitter_massUncorr = JetFitter_massUncorr_acc(*btagging);
  out.JetFitter_dRFlightDir = JetFitter_dRFlightDir_acc(*btagging);
  out.JetFitter_nVTX = JetFitter_nVTX_acc(*btagging);
  out.JetFitter_nSingleTracks = JetFitter_nSingleTracks_acc(*btagging);
  out.JetFitter_nTracksAtVtx = JetFitter_nTracksAtVtx_acc(*btagging);
  out.JetFitter_N2Tpair = JetFitter_N2Tpair_acc(*btagging);

  out.SV1_masssvx = SV1_masssvx_acc(*btagging);
  out.SV1_efracsvx = SV1_efracsvx_acc(*btagging);
  out.SV1_significance3d = SV1_significance3d_acc(*btagging);
  out.SV1_dstToMatLay = SV1_dstToMatLay_acc(*btagging);
  out.SV1_deltaR = SV1_deltaR_acc(*btagging);
  out.SV1_Lxy = SV1_Lxy_acc(*btagging);
  out.SV1_L3d = SV1_L3d_acc(*btagging);
  out.SV1_N2Tpair = SV1_N2Tpair_acc(*btagging);
  out.SV1_NGTinSvx = SV1_NGTinSvx_acc(*btagging);

  return;
}


void largeJetBranches(TTree* t, LargeJet& lj) {
  t->Branch("largejet_pt", &(lj.pt));
  t->Branch("largejet_eta", &(lj.eta));
  t->Branch("largejet_phi", &(lj.phi));
  t->Branch("largejet_m", &(lj.m));
  t->Branch("largejet_label", &(lj.label));

  return;
}


void subjetBranches(TTree* t, Subjet& sj, const std::string& prefix) {
  t->Branch((prefix + "_pt").c_str(), &(sj.pt));
  t->Branch((prefix + "_eta").c_str(), &(sj.eta));
  t->Branch((prefix + "_phi").c_str(), &(sj.phi));
  t->Branch((prefix + "_m").c_str(), &(sj.m));

  t->Branch((prefix + "_MV2c10rnn_discriminant").c_str(), &(sj.MV2c10rnn_discriminant));

  t->Branch((prefix + "_DL1rnn_pu").c_str(), &(sj.DL1rnn_pu));
  t->Branch((prefix + "_DL1rnn_pc").c_str(), &(sj.DL1rnn_pc));
  t->Branch((prefix + "_DL1rnn_pb").c_str(), &(sj.DL1rnn_pb));

  t->Branch((prefix + "_rnnip_pu").c_str(), &(sj.rnnip_pu));
  t->Branch((prefix + "_rnnip_pc").c_str(), &(sj.rnnip_pc));
  t->Branch((prefix + "_rnnip_pb").c_str(), &(sj.rnnip_pb));
  t->Branch((prefix + "_rnnip_ptau").c_str(), &(sj.rnnip_ptau));

  t->Branch((prefix + "_JetFitter_energyFraction").c_str(), &(sj.JetFitter_energyFraction));
  t->Branch((prefix + "_JetFitter_mass").c_str(), &(sj.JetFitter_mass));
  t->Branch((prefix + "_JetFitter_significance3d").c_str(), &(sj.JetFitter_significance3d));
  t->Branch((prefix + "_JetFitter_deltaphi").c_str(), &(sj.JetFitter_deltaphi));
  t->Branch((prefix + "_JetFitter_deltaeta").c_str(), &(sj.JetFitter_deltaeta));
  t->Branch((prefix + "_JetFitter_massUncorr").c_str(), &(sj.JetFitter_massUncorr));
  t->Branch((prefix + "_JetFitter_dRFlightDir").c_str(), &(sj.JetFitter_dRFlightDir));
  t->Branch((prefix + "_JetFitter_nVTX").c_str(), &(sj.JetFitter_nVTX));
  t->Branch((prefix + "_JetFitter_nSingleTracks").c_str(), &(sj.JetFitter_nSingleTracks));
  t->Branch((prefix + "_JetFitter_nTracksAtVtx").c_str(), &(sj.JetFitter_nTracksAtVtx));
  t->Branch((prefix + "_JetFitter_N2Tpair").c_str(), &(sj.JetFitter_N2Tpair));

  t->Branch((prefix + "_SV1_masssvx").c_str(), &(sj.SV1_masssvx));
  t->Branch((prefix + "_SV1_efracsvx").c_str(), &(sj.SV1_efracsvx));
  t->Branch((prefix + "_SV1_significance3d").c_str(), &(sj.SV1_significance3d));
  t->Branch((prefix + "_SV1_dstToMatLay").c_str(), &(sj.SV1_dstToMatLay));
  t->Branch((prefix + "_SV1_deltaR").c_str(), &(sj.SV1_deltaR));
  t->Branch((prefix + "_SV1_Lxy").c_str(), &(sj.SV1_Lxy));
  t->Branch((prefix + "_SV1_L3d").c_str(), &(sj.SV1_L3d));
  t->Branch((prefix + "_SV1_N2Tpair").c_str(), &(sj.SV1_N2Tpair));
  t->Branch((prefix + "_SV1_NGTinSvx").c_str(), &(sj.SV1_NGTinSvx));

  return;
}

int main (int argc, char *argv[])
{
  const IOOpts opts = get_io_opts(argc, argv);
  const HbbConfig jobcfg = get_hbb_config(opts.config_file_name);

  // The name of the application:
  static const char *APP_NAME = "BTagTestDumper";

  // Set up the environment:
  RETURN_CHECK( APP_NAME, xAOD::Init() );

  // Set up the event object:
  xAOD::TEvent event(xAOD::TEvent::kClassAccess);

  // Initialize JetCalibrationTool with release 21 recommendations
  JetCalibrationTool calib_tool("JetCalibrationTool");
  calib_tool.setProperty("JetCollection", jobcfg.jet_collection);
  calib_tool.setProperty("ConfigFile", jobcfg.jet_calib_file);
  calib_tool.setProperty("CalibSequence", jobcfg.cal_seq);
  calib_tool.setProperty("CalibArea", jobcfg.cal_area);
  calib_tool.setProperty("IsData", false);
  RETURN_CHECK( APP_NAME, calib_tool.initialize() );

  // define output file output files
  TFile *fout = new TFile(opts.out.c_str(), "RECREATE");
  fout->cd();

  TH2F* hpteta_sig = new TH2F("hpteta_sig", "hpteta_sig", 53, 350000, 3000000, 20, 0, 2);
  TH2F* hpteta_bkg = new TH2F("hpteta_bkg", "hpteta_bkg", 53, 350000, 3000000, 20, 0, 2);

  TTree* wtree = new TTree("weights", "weights");
  float sumOfWeights = 0;
  int dsid = 0;
  wtree->Branch("sumOfWeights", &sumOfWeights);
  wtree->Branch("dsid", &dsid);

  TTree* tree = new TTree("toptagger", "toptagger");
  LargeJet lj;
  Subjet sj0;
  Subjet sj1;
  Subjet sj2;

  largeJetBranches(tree, lj);
  subjetBranches(tree, sj0, "subjet0");
  subjetBranches(tree, sj1, "subjet1");
  subjetBranches(tree, sj2, "subjet2");

  int mcChannelNumber;
  tree->Branch("mcChannelNumber", &mcChannelNumber);
  float weight;
  tree->Branch("weight", &weight);

  Counts counts;

  // Loop over the specified files:
  for (const std::string& file: opts.in) {

    // Open the file:
    std::unique_ptr<TFile> ifile(TFile::Open(file.c_str(), "READ"));
    if ( ! ifile.get() || ifile->IsZombie()) {
      Error( APP_NAME, "Couldn't open file: %s", file.c_str() );
      return 1;
    }
    Info( APP_NAME, "Opened file: %s", file.c_str() );

    // Connect the event object to it:
    RETURN_CHECK( APP_NAME, event.readFrom(ifile.get()) );

    // Loop over its events:
    const unsigned long long entries = event.getEntries();
    if (entries > 0) {
      event.getEntry(0);
      counts += get_counts(*ifile, event);
    }

    for (unsigned long long entry = 0; entry < entries; ++entry) {
      if (opts.n_entries && entry > opts.n_entries) break;

      // Load the event:
      if (event.getEntry(entry) < 0) {
        Error( APP_NAME, "Couldn't load entry %lld from file: %s",
            entry, file.c_str() );
        return 1;
      }

      // Print some status:
      if ( ! (entry % 500)) {
        Info( APP_NAME, "Processing entry %lld / %lld", entry, entries );
      }

      const xAOD::EventInfo* event_info = 0;
      RETURN_CHECK( APP_NAME, event.retrieve(event_info, "EventInfo") );

      weight = event_info->mcEventWeight();
      mcChannelNumber = event_info->mcChannelNumber();

      const xAOD::JetContainer *raw_jets = 0;
      auto full_collection = jobcfg.jet_collection + "Jets";
      RETURN_CHECK( APP_NAME, event.retrieve(raw_jets, full_collection) );

      for (const xAOD::Jet *raw_jet : *raw_jets) { // loop over large-R jets
        xAOD::Jet* jet;
        calib_tool.calibratedCopy(*raw_jet, jet);

        if (!jet) {
          std::cout << "missing jet after calibration!" << std::endl;

          delete jet;
          return -1;
        }

        size_t nsj = subjet_acc(*jet).size();
        if (jet->pt() < 350000 || std::abs(jet->eta()) > 2.0 || nsj < 3) {
          delete jet;
          continue;
        }

        largejet(jet, lj);
        subjet(jet, 0, sj0);
        subjet(jet, 1, sj1);
        subjet(jet, 2, sj2);

        // check for third subjet just being a ghost constituent
        if (sj2.pt < 1) {
          delete jet;
          continue;
        }


        tree->Fill();

        if (lj.label)
          hpteta_sig->Fill(jet->pt(), std::abs(jet->eta()), weight);
        else
          hpteta_bkg->Fill(jet->pt(), std::abs(jet->eta()), weight);

        delete jet;
      } // end jet loop

    } // end event loop
  } // end file loop

  sumOfWeights = counts.sumOfWeights;
  dsid = mcChannelNumber;
  wtree->Fill();

  tree->Write();
  wtree->Write();
  hpteta_sig->Write();
  hpteta_bkg->Write();
  fout->Write();
  fout->Close();

  return 0;
}
