#include "HbbConfig.hh"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace {
  bool boolinate(const boost::property_tree::ptree& pt,
                 const std::string key) {
    std::string val = pt.get<std::string>(key);
    if (val == "true") return true;
    if (val == "false") return false;
    throw std::logic_error("'" + key + "' should be 'true' or 'false'");
  }
}

HbbConfig get_hbb_config(const std::string& config_file_name) {
  HbbConfig config;

  boost::property_tree::ptree pt;
  boost::property_tree::read_json(config_file_name, pt);

  for (const auto& subnode: pt.get_child("subjets")) {
    const auto& subjet = subnode.second;
    SubjetConfig cfg;
    cfg.input_name = subjet.get<std::string>("input_name");
    cfg.output_name = subjet.get<std::string>("output_name");
    cfg.store_tracks = boolinate(subjet,"store_tracks");
    cfg.get_subjets_from_parent = boolinate(subjet,"get_subjets_from_parent");
    cfg.n_subjets_to_save = subjet.get<size_t>("n_subjets_to_save");
    config.subjet_configs.push_back(cfg);
  }
  config.jet_collection = pt.get<std::string>("jet_collection");
  config.jet_calib_file = pt.get<std::string>("jet_calib_file");
  config.cal_seq = pt.get<std::string>("cal_seq");
  config.cal_area = pt.get<std::string>("cal_area");
  return config;
}
