#ifndef SINGLE_BTAG_OPTIONS_HH
#define SINGLE_BTAG_OPTIONS_HH

#include <vector>
#include <string>

struct SingleTagIOOpts
{
  std::vector<std::string> in;
  std::string out;
  bool save_tracks;
  unsigned long long max_events;
};

SingleTagIOOpts get_single_tag_io_opts(int argc, char* argv[]);


#endif
