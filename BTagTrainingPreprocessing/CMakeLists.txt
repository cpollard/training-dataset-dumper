#
# Build configuration for the IOTests package
#

# Set the name of the package:
atlas_subdir( BTagTrainingPreprocessing )

# Packages that this package depends on:
atlas_depends_on_subdirs(
  PRIVATE
  Control/xAODRootAccess
  Event/xAOD/xAODEventInfo
  Event/xAOD/xAODCaloCluster
  Event/xAOD/xAODTrack
  Event/xAOD/xAODTruth
  Event/xAOD/xAODCutFlow
  PhysicsAnalysis/AnalysisCommon/HDF5Utils
  PhysicsAnalysis/JetTagging/FlavorTagDiscriminants
  Control/AthToolSupport/AsgTools
  Reconstruction/Jet/JetCalibTools
  Reconstruction/Jet/JetSelectorTools
  Reconstruction/Jet/BoostedJetTaggers
  InnerDetector/InDetRecTools/InDetTrackSelectionTool
  Reconstruction/Jet/JetMomentTools
)

# External(s) used by the package:
find_package(ROOT REQUIRED COMPONENTS RIO Hist Tree Net Core)
find_package(HDF5 1.10.1 REQUIRED COMPONENTS CXX C)

# common requirements
set(_common
  src/BTagJetWriter.cxx
  src/BTagROOTJetFiller.cxx
  src/BTagJetWriterConfig.cxx
  src/BTagTrackWriter.cxx
  src/BTagROOTTrackFiller.cxx
  src/BookKeeper.cxx
  src/addMetadata.cxx
  src/TrackSelector.cxx
  INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} src
  LINK_LIBRARIES ${ROOT_LIBRARIES} ${HDF5_LIBRARIES}
  xAODRootAccess
  xAODCaloEvent
  xAODTracking
  xAODJet
  xAODTruth
  HDF5Utils
  xAODCutFlow
  AsgTools
  JetCalibToolsLib
  JetSelectorToolsLib
  InDetTrackSelectionToolLib
  JetMomentToolsLib
  BoostedJetTaggersLib
  FlavorTagDiscriminants
)

# Build the test executable:
atlas_add_executable( dump-single-btag
  util/dump-single-btag.cxx util/SingleBTagOptions.cxx ${_common})
atlas_add_executable( dump-hbb
  util/dump-hbb.cxx util/HbbOptions.cxx util/HbbConfig.cxx ${_common})
atlas_add_executable( reweight-hbb
  util/reweight-hbb.cxx ${_common})
