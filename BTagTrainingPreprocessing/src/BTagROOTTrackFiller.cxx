#include "BTagROOTTrackFiller.hh"

#include <cstddef>

#include "xAODTracking/TrackParticle.h"
#include "xAODJet/Jet.h"

#include "TTree.h"

BTagROOTTrackFiller::BTagROOTTrackFiller(TTree &output_tree, const BTagTrackWriterConfig &config) : m_config(config) {
  output_tree.Branch("track_pt", &m_pt);
  output_tree.Branch("track_eta", &m_eta);

  output_tree.Branch("track_deltaEta", &m_deltaEta);
  output_tree.Branch("track_deltaPhi", &m_deltaPhi);
  output_tree.Branch("track_deltaR", &m_deltaR);
  output_tree.Branch("track_ptFrac", &m_ptFrac);

  set_up_variable_group_branches(output_tree, m_uchar_variable_vectors, config.uchar_variables);
  set_up_variable_group_branches(output_tree, m_int_variable_vectors, config.int_variables);
  set_up_variable_group_branches(output_tree, m_float_variable_vectors, config.float_variables);
  set_up_variable_group_branches(output_tree, m_double_variable_vectors, config.double_variables);
}

BTagROOTTrackFiller::~BTagROOTTrackFiller() {
  for (auto *variable_vector : m_uchar_variable_vectors) {
    delete variable_vector;
  }
  for (auto *variable_vector : m_int_variable_vectors) {
    delete variable_vector;
  }
  for (auto *variable_vector : m_float_variable_vectors) {
    delete variable_vector;
  }
  for (auto *variable_vector : m_double_variable_vectors) {
    delete variable_vector;
  }
}

template<typename T>
void BTagROOTTrackFiller::set_up_variable_group_branches(TTree &tree, std::vector<std::vector<T> *> &variable_vectors, const std::vector<std::string> &variable_names) {
  for (const std::string &variable_name : variable_names) {
    variable_vectors.push_back(new std::vector<T>);
    tree.Branch(("track_" + variable_name).c_str(), variable_vectors.back());
  }
}

void BTagROOTTrackFiller::clear_variable_vectors() {
  m_pt.clear();
  m_eta.clear();

  m_deltaEta.clear();
  m_deltaPhi.clear();
  m_deltaR.clear();
  m_ptFrac.clear();

  for (auto *variable_vector : m_uchar_variable_vectors) {
    variable_vector->clear();
  }
  for (auto *variable_vector : m_int_variable_vectors) {
    variable_vector->clear();
  }
  for (auto *variable_vector : m_float_variable_vectors) {
    variable_vector->clear();
  }
  for (auto *variable_vector : m_double_variable_vectors) {
    variable_vector->clear();
  }
}

void BTagROOTTrackFiller::fill(const std::vector<const xAOD::TrackParticle *> &tracks, const xAOD::Jet &jet) {
  clear_variable_vectors();
  for (std::size_t track_index = 0; track_index < tracks.size() && track_index < m_config.output_size.at(0); ++track_index) {
    const xAOD::TrackParticle &track = *tracks.at(track_index);

    m_pt.push_back(track.pt());
    m_eta.push_back(track.eta());

    const float deltaEta = track.eta() - jet.eta();
    const float deltaPhi = track.p4().DeltaPhi(jet.p4());

    m_deltaEta.push_back(deltaEta);
    m_deltaPhi.push_back(deltaPhi);
    m_deltaR.push_back(std::hypot(deltaEta, deltaPhi));
    m_ptFrac.push_back(track.pt() / jet.pt());

    fill_variable_group(track, m_uchar_variable_vectors, m_config.uchar_variables);
    fill_variable_group(track, m_int_variable_vectors, m_config.int_variables);
    fill_variable_group(track, m_float_variable_vectors, m_config.float_variables);
    fill_variable_group(track, m_double_variable_vectors, m_config.double_variables);
  }
}


template<typename T>
void BTagROOTTrackFiller::fill_variable_group(const xAOD::TrackParticle &track, std::vector<std::vector<T> *> &variable_vectors, const std::vector<std::string> &variable_names) {
  for (std::size_t variable_index = 0; variable_index < variable_names.size(); ++variable_index) {
    const std::string &variable_name = variable_names.at(variable_index);
    variable_vectors.at(variable_index)->push_back(track.auxdata<T>(variable_name));
  }
}
