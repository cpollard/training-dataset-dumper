#include "BTagTrackWriter.hh"
#include "BTagTrackWriterConfig.hh"
#include "HDF5Utils/HdfTuple.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODTracking/TrackParticle.h"
#include "xAODJet/Jet.h"

BTagTrackWriter::BTagTrackWriter(
  H5::Group& output_file,
  const BTagTrackWriterConfig& config):
  m_current_tracks(),
  m_empty_track_container(new std::vector<const xAOD::TrackParticle*>()),
  m_current_jet(nullptr),
  m_hdf5_track_writer(nullptr)
{

  H5Utils::VariableFillers fillers;
  // add the variables passed as strings
  add_track_fillers<float>(fillers, config.float_variables, NAN);
  // We can convert the doubles to floats to save space. Change the
  // float below to a double if you want to save with full precision.
  add_track_fillers<double, float>(fillers, config.double_variables, NAN);
  add_track_fillers<int>(fillers, config.int_variables, -1);
  add_track_fillers<unsigned char, unsigned char>(fillers, config.uchar_variables, 0);

  // hard coded 4 momentum fillers
  std::function<float(void)> pt = [this]() -> float {
    size_t index = this->m_track_index.at(0);
    if (index >= m_current_tracks->size()) return NAN;
    return m_current_tracks->at(index)->pt();
  };
  fillers.add("pt", pt);
  std::function<float(void)> eta = [this]() -> float {
    size_t index = this->m_track_index.at(0);
    if (index >= m_current_tracks->size()) return NAN;
    return m_current_tracks->at(index)->eta();
  };
  fillers.add("eta", eta);

  add_rel_jet_kinematics(fillers);

  // build the output dataset
  assert(config.name.size() > 0);
  assert(config.output_size.size() == 1);
  std::vector<hsize_t> size(config.output_size.begin(),
                            config.output_size.end());
  if (config.output_size.at(0) > 0) {
    m_hdf5_track_writer = new H5Utils::WriterXd(output_file, config.name,
                                                fillers, size);
  }
}

BTagTrackWriter::~BTagTrackWriter() {
  if (m_hdf5_track_writer) m_hdf5_track_writer->flush();
  delete m_hdf5_track_writer;
  delete m_empty_track_container;
}

void BTagTrackWriter::write(const BTagTrackWriter::Tracks& tracks,
                            const xAOD::Jet& jet) {
  m_current_tracks = &tracks;
  m_current_jet = &jet;
  if (m_hdf5_track_writer) {
    m_hdf5_track_writer->fillWhileIncrementing(m_track_index);
  }
}
void BTagTrackWriter::write_dummy() {
  m_current_tracks = m_empty_track_container;
  m_current_jet = nullptr;
  if (m_hdf5_track_writer) {
    m_hdf5_track_writer->fillWhileIncrementing(m_track_index);
  }
}

template<typename I, typename O>
void BTagTrackWriter::add_track_fillers(H5Utils::VariableFillers& vars,
                                      const std::vector<std::string>& names,
                                      O def_value) {

  for (const auto& btag_var: names) {
    std::function<O(void)> filler = [this, btag_var, def_value]() -> O {
      size_t index = this->m_track_index.at(0);
      if (index >= m_current_tracks->size()) {
        return def_value;
      }
      const xAOD::TrackParticle *trk = m_current_tracks->at(index);
      return trk->auxdata<I>(btag_var);
    };
    vars.add(btag_var, filler);
  }
}

void BTagTrackWriter::add_rel_jet_kinematics(H5Utils::VariableFillers& vars) {

  std::function<float(void)> deta = [this]() -> float {
    size_t index = this->m_track_index.at(0);
    if (index >= m_current_tracks->size() ) {
      return NAN;
    }
    const auto* trk = m_current_tracks->at(index);
    return trk->eta() - this->m_current_jet->eta();
  };
  vars.add("deta", deta);

  std::function<float(void)> dphi = [this]() -> float {
    size_t index = this->m_track_index.at(0);
    if (index >= m_current_tracks->size() ) {
      return NAN;
    }
    const auto* trk = m_current_tracks->at(index);
    return trk->p4().DeltaPhi(this->m_current_jet->p4());
  };
  vars.add("dphi", dphi);

  std::function<float(void)> dr = [this]() -> float {
    size_t index = this->m_track_index.at(0);
    if (index >= m_current_tracks->size() ) {
      return NAN;
    }
    const auto* trk = m_current_tracks->at(index);
    return trk->p4().DeltaR(this->m_current_jet->p4());
  };
  vars.add("dr", dr);

  std::function<float(void)> ptfrac = [this]() -> float {
    size_t index = this->m_track_index.at(0);
    if (index >= m_current_tracks->size() ) {
      return NAN;
    }
    const auto* trk = m_current_tracks->at(index);
    return trk->pt() / this->m_current_jet->pt();
  };
  vars.add("ptfrac", ptfrac);

}
