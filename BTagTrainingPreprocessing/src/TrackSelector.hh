#ifndef TRACK_SELECTOR_HH
#define TRACK_SELECTOR_HH

#include "xAODTracking/TrackParticleContainer.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"
#include "xAODJet/Jet.h"

class TrackSelector
{
public:
  TrackSelector();
  typedef std::vector<const xAOD::TrackParticle*> Tracks;
  Tracks get_tracks(const xAOD::Jet& jet) const;
private:
  typedef SG::AuxElement AE;
  typedef std::vector<ElementLink<xAOD::TrackParticleContainer> > TrackLinks;
  AE::ConstAccessor<TrackLinks> m_track_associator;

  InDet::InDetTrackSelectionTool m_track_selector;
};

#endif
