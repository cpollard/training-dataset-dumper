#include "TrackSelector.hh"
#include "xAODJet/Jet.h"

TrackSelector::TrackSelector():
  m_track_associator("BTagTrackToJetAssociator"),
  m_track_selector("InDetTrackSelectionTool", "Loose")
{
  if (!m_track_selector.initialize()) {
    throw std::logic_error("can't initialize track seletor");
  }
}

TrackSelector::Tracks TrackSelector::get_tracks(const xAOD::Jet& jet) const
{
  const xAOD::BTagging *btagging = jet.btagging();
  std::vector<const xAOD::TrackParticle*> tracks;
  for (const auto &link : m_track_associator(*btagging)) {
    if(link.isValid()) {
      const xAOD::TrackParticle *tp = *link;
      if (m_track_selector.accept(tp)) {
        tracks.push_back(tp);
      }
    } else {
      throw std::logic_error("invalid track link");
    }
  }
  return tracks;
}
