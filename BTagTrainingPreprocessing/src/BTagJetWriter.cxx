#include "BTagJetWriter.hh"
#include "BTagJetWriterConfig.hh"
#include "HDF5Utils/HdfTuple.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"

struct CurrentObjectBasepoint {
  CurrentObjectBasepoint();
  const xAOD::Jet* jet;
  const xAOD::Jet* parent;
  const xAOD::EventInfo* event_info;
};

CurrentObjectBasepoint::CurrentObjectBasepoint():
  jet(nullptr),
  parent(nullptr),
  event_info(nullptr)
{}

struct SubstructureAccessors {
  typedef SG::AuxElement AE;
  SubstructureAccessors();
  float c2(const xAOD::Jet&);
  float d2(const xAOD::Jet&);
  float e3(const xAOD::Jet&);
  float tau21(const xAOD::Jet&);
  float tau32(const xAOD::Jet&);
  float fw20(const xAOD::Jet&);

  AE::ConstAccessor<float> ecf1;
  AE::ConstAccessor<float> ecf2;
  AE::ConstAccessor<float> ecf3;

  AE::ConstAccessor<float> tau1wta;
  AE::ConstAccessor<float> tau2wta;
  AE::ConstAccessor<float> tau3wta;

  AE::ConstAccessor<float> fw2;
  AE::ConstAccessor<float> fw0;
};

SubstructureAccessors::SubstructureAccessors():
  ecf1("ECF1"), ecf2("ECF2"), ecf3("ECF3"),
  tau1wta("Tau1_wta"), tau2wta("Tau2_wta"), tau3wta("Tau3_wta"),
  fw2("FoxWolfram2"), fw0("FoxWolfram0")
{}
float SubstructureAccessors::c2(const xAOD::Jet& j) {
  return ecf3(j) * ecf1(j) / pow(ecf2(j), 2.0);
}
float SubstructureAccessors::d2(const xAOD::Jet& j) {
  return ecf3(j) * pow(ecf1(j), 3.0) / pow(ecf2(j), 3.0);
}
float SubstructureAccessors::e3(const xAOD::Jet& j) {
  return ecf3(j)/pow(ecf1(j),3.0);
}
float SubstructureAccessors::tau21(const xAOD::Jet& j) {
  return tau2wta(j) / tau1wta(j);
}
float SubstructureAccessors::tau32(const xAOD::Jet& j) {
  return tau3wta(j) / tau2wta(j);
}
float SubstructureAccessors::fw20(const xAOD::Jet& j) {
  return fw2(j) / fw0(j);
}

BTagJetWriter::BTagJetWriter(
  H5::Group& output_file,
  const BTagJetWriterConfig& config):
  m_current(new CurrentObjectBasepoint),
  m_write_parent(config.write_kinematics_relative_to_parent ||
                 config.parent_truth_labels.size() > 0),
  m_ssa(new SubstructureAccessors)
{

  // create the variable fillers
  H5Utils::VariableFillers fillers;
  if ( (config.double_variables.size()
        + config.float_variables.size()
        + config.int_variables.size() ) > 0) {
        // in this case we convert doubles to floats to save space,
        // note that you can change the second parameter to a double
        // to get full precision.
    const BTagVariableMaps& m = config.variable_maps;
    add_btag_fillers<double, float>(fillers, config.double_variables, m, NAN);
    add_btag_fillers<float>(fillers, config.float_variables, m, NAN);
    add_btag_fillers<int>(fillers, config.int_variables, m, -1);
    add_btag_fillers<char>(fillers, config.char_variables, m, -1);
    add_btag_fillers<int, float>(
      fillers, config.int_as_float_variables, m, NAN);
  }
  if (config.jet_float_variables.size() > 0) {
    add_jet_fillers<float>(fillers, config.jet_float_variables, NAN);
  }
  add_truth_labels(fillers, config.truth_labels);


  // some things like 4 momenta have to be hand coded
  std::function<float(void)> pt = [current=m_current]() -> float {
    if (!current->jet) return NAN;
    return current->jet->pt();
  };
  fillers.add("pt", pt);

  std::function<float(void)> eta = [current=m_current]() -> float {
    if (!current->jet) return NAN;
    return current->jet->eta();
  };
  fillers.add("eta", eta);

  if (m_write_parent) {
    if (config.write_kinematics_relative_to_parent) {
      add_parent_fillers(fillers);
    }
    if (config.parent_truth_labels.size() > 0) {
      add_parent_truth_labels(fillers, config.parent_truth_labels);
    }
  }

  if (config.write_event_info) add_event_info(fillers);
  if (config.write_substructure_moments) add_substructure(fillers);

  // now create the writer
  assert(config.name.size() > 0);
  m_hdf5_jet_writer = new H5Utils::WriterXd(
    output_file, config.name, fillers,{});
  assert(config.output_size.size() == 0);
}

BTagJetWriter::~BTagJetWriter() {
  if (m_hdf5_jet_writer) m_hdf5_jet_writer->flush();
  delete m_current;
  delete m_hdf5_jet_writer;
  delete m_ssa;
}

BTagJetWriter::BTagJetWriter(BTagJetWriter&& old):
  m_current(old.m_current),
  m_write_parent(old.m_write_parent),
  m_hdf5_jet_writer(old.m_hdf5_jet_writer),
  m_ssa(old.m_ssa)
{
  old.m_current = nullptr;
  old.m_hdf5_jet_writer = nullptr;
  old.m_ssa = nullptr;
}

void BTagJetWriter::write(const xAOD::Jet& jet,
                          const xAOD::EventInfo* mc_event_info) {
  assert(!m_write_parent);
  m_current->event_info = mc_event_info;
  m_current->jet = &jet;
  m_hdf5_jet_writer->fillWhileIncrementing();
}

void BTagJetWriter::write_dummy() {
  m_current->event_info = nullptr;
  m_current->jet = nullptr;
  m_current->parent = nullptr;
  m_hdf5_jet_writer->fillWhileIncrementing();
}

void BTagJetWriter::write_with_parent(const xAOD::Jet& jet,
                                      const xAOD::Jet& parent,
                                      const xAOD::EventInfo* mc_event_info) {
  assert(m_write_parent);
  m_current->event_info = mc_event_info;
  m_current->jet = &jet;
  m_current->parent = &parent;
  m_hdf5_jet_writer->fillWhileIncrementing();
}


template<typename I, typename O>
void BTagJetWriter::add_btag_fillers(
  H5Utils::VariableFillers& vars,
  const std::vector<std::string>& names,
  const BTagVariableMaps& maps,
  O default_value) {
  const auto& replace_with_defaults_checks = maps.replace_with_defaults_checks;
  for (const auto& btag_var: names) {
    std::string out_name = btag_var;
    if (maps.rename.count(btag_var)) out_name = maps.rename.at(btag_var);
    if (replace_with_defaults_checks.count(btag_var)) {
      std::string replace_with_default_check = replace_with_defaults_checks.at(btag_var);
      std::function<O(void)> filler = [
        current=m_current, btag_var, default_value, replace_with_default_check]() -> O {
        if (!current->jet) return default_value;
        auto* btagging = current->jet->btagging();
        if (!btagging) {
          throw std::logic_error("missing b-tagging container");
        }
        if (btagging->auxdata<char>(replace_with_default_check)) return default_value;
        return btagging->auxdata<I>(btag_var);
      };
      vars.add(out_name, filler);
    } else {
      std::function<O(void)> filler = [
        current=m_current, btag_var, default_value]() -> O {
        if (!current->jet) return default_value;
        auto* btagging = current->jet->btagging();
        if (!btagging) {
          throw std::logic_error("missing b-tagging container");
        }
        return btagging->auxdata<I>(btag_var);
      };
      vars.add(out_name, filler);
    }
  }
}
template<typename I, typename O>
void BTagJetWriter::add_jet_fillers(H5Utils::VariableFillers& vars,
                                    const std::vector<std::string>& names,
                                    O default_value) {
  for (const auto& btag_var: names) {
    std::function<O(void)> filler = [
      current=m_current, btag_var, default_value]() -> O {
      if (!current->jet) return default_value;
      return current->jet->auxdata<I>(btag_var);
    };
    vars.add(btag_var, filler);
  }
}

void BTagJetWriter::add_truth_labels(H5Utils::VariableFillers& vars,
                                     const std::vector<std::string>& names) {
  for (const auto& label: names) {
    std::function<int(void)> filler = [current=m_current, label]() {
      if (!current->jet) return -1;
      return current->jet->auxdata<int>(label);
    };
    vars.add(label, filler);
  }
}
void BTagJetWriter::add_parent_truth_labels(
  H5Utils::VariableFillers& vars,
  const std::vector<std::string>& names) {
  for (const auto& label: names) {
    std::function<int(void)> filler = [current=m_current, label]() {
      if (!current->parent) return -1;
      return current->parent->auxdata<int>(label);
    };
    vars.add(label, filler);
  }
}

void BTagJetWriter::add_parent_fillers(H5Utils::VariableFillers& vars) {
  std::function<float(void)> deta = [current=m_current]() -> float {
    if (!current->jet || !current->parent) return NAN;
    return current->jet->eta() - current->parent->eta();
  };
  vars.add("deta", deta);
  std::function<float(void)> dphi = [current=m_current]() -> float {
    if (!current->jet || !current->parent) return NAN;
    return current->jet->p4().DeltaPhi(current->parent->p4());
  };
  vars.add("dphi", dphi);
  std::function<float(void)> dr = [current=m_current]() -> float {
    if (!current->jet || !current->parent) return NAN;
    return current->jet->p4().DeltaR(current->parent->p4());
  };
  vars.add("dr", dr);
}

void BTagJetWriter::add_substructure(H5Utils::VariableFillers& vars) {
  std::function<float(void)> mass = [current=m_current]() {
    return current->jet->m();
  };
  vars.add("mass", mass);

  std::function<float(void)> c2 = [ssa=m_ssa, current=m_current]() {
    return ssa->c2(*current->jet);
  };
  vars.add("C2", c2);

  std::function<float(void)> d2 = [ssa=m_ssa, current=m_current]() {
    return ssa->d2(*current->jet);
  };
  vars.add("D2", d2);

  std::function<float(void)> e3 = [ssa=m_ssa, current=m_current]() {
    return ssa->e3(*current->jet);
  };
  vars.add("e3", e3);

  std::function<float(void)> tau21 = [ssa=m_ssa, current=m_current]() {
    return ssa->tau21(*current->jet);
  };
  vars.add("Tau21_wta", tau21);

  std::function<float(void)> tau32 = [ssa=m_ssa, current=m_current]() {
    return ssa->tau32(*current->jet);
  };
  vars.add("Tau32_wta", tau32);

  std::function<float(void)> fw20 = [ssa=m_ssa, current=m_current]() {
    return ssa->fw20(*current->jet);
  };
  vars.add("FoxWolfram20", fw20);

}

void BTagJetWriter::add_event_info(H5Utils::VariableFillers& vars) {
  std::function<float(void)> evt_weight = [current=m_current]() {
    assert(current->event_info);
    return current->event_info->mcEventWeight();
  };
  std::function<long long(void)> evt_number = [current=m_current]() {
    return current->event_info->eventNumber();
  };
  vars.add("mcEventWeight", evt_weight);
  vars.add("eventNumber", evt_number);
}
