#ifndef BTAG_ROOT_TRACK_FILLER_HH
#define BTAG_ROOT_TRACK_FILLER_HH

#include <vector>

#include "BTagTrackWriterConfig.hh"

namespace xAOD {
  class TrackParticle_v1;
  typedef TrackParticle_v1 TrackParticle;
  class Jet_v1;
  typedef Jet_v1 Jet;
}

class TTree;

class BTagROOTTrackFiller {
  public:
    BTagROOTTrackFiller(TTree &output_tree, const BTagTrackWriterConfig &config);
    ~BTagROOTTrackFiller();
    void fill(const std::vector<const xAOD::TrackParticle *> &tracks, const xAOD::Jet &jet);
  private:
    template<typename T>
    void set_up_variable_group_branches(TTree &tree, std::vector<std::vector<T> *> &variable_vectors, const std::vector<std::string> &variable_names);
    void clear_variable_vectors();
    template<typename T>
    void fill_variable_group(const xAOD::TrackParticle &track, std::vector<std::vector<T> *> &variable_vectors, const std::vector<std::string> &variable_names);

    const BTagTrackWriterConfig m_config;

    std::vector<float> m_pt;
    std::vector<float> m_eta;

    std::vector<float> m_deltaEta;
    std::vector<float> m_deltaPhi;
    std::vector<float> m_deltaR;
    std::vector<float> m_ptFrac;

    std::vector<std::vector<unsigned char> *> m_uchar_variable_vectors;
    std::vector<std::vector<int> *> m_int_variable_vectors;
    std::vector<std::vector<float> *> m_float_variable_vectors;
    std::vector<std::vector<double> *> m_double_variable_vectors;
};

#endif
