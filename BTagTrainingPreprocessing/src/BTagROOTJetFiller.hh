#ifndef BTAG_ROOT_JET_FILLER_HH
#define BTAG_ROOT_JET_FILLER_HH

#include <vector>

#include "BTagJetWriterConfig.hh"

namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
  class EventInfo_v1;
  typedef EventInfo_v1 EventInfo;
  class BTagging_v1;
  typedef BTagging_v1 BTagging;
}

class TTree;

class BTagROOTJetFiller {
  public:
    BTagROOTJetFiller(TTree &output_tree, const BTagJetWriterConfig &config);
    ~BTagROOTJetFiller();
    void fill(const xAOD::Jet &jet, const xAOD::EventInfo *event_info);
  private:
    template<typename T>
    void set_up_variable_group_branches(TTree &tree, std::vector<T *> &variables, const std::vector<std::string> &variable_names);
    template<typename T>
    void fill_jet_variable_group(const xAOD::Jet &jet, std::vector<T *> &variables, const std::vector<std::string> &variable_names);
    template<typename I, typename O>
    void fill_btagging_variable_group(const xAOD::BTagging &btagging, std::vector<O *> &variables, const std::vector<std::string> &variable_names, const O default_value);

    BTagJetWriterConfig m_config;

    float m_eventNumber;
    float m_mcEventWeight;

    float m_pt;
    float m_eta;

    std::vector<int *> m_jet_int_variables;
    std::vector<float *> m_jet_float_variables;

    std::vector<char *> m_btagging_char_variables;
    std::vector<int *> m_btagging_int_variables;
    std::vector<float *> m_btagging_int_as_float_variables;
    std::vector<float *> m_btagging_float_variables;
    std::vector<double *> m_btagging_double_variables;
};

#endif
