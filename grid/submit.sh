#!/usr/bin/env bash

# This script should not be sourced, we don't need anything in here to
# propigate to the surrounding environment.
if [[ $- == *i* ]] ; then
    echo "Don't source me!" >&2
    return 1
else
  # set the shell to exit if there's an error (-e), and to error if
  # there's an unset variable (-u)
    set -eu
fi


##########################
# Real things start here #
##########################

###################################################
# Part 1: variables you you _might_ need to change
###################################################
#
# Users's grid name
GRID_NAME=${RUCIO_ACCOUNT-${USER}}
#
# This job's tag (the current expression is something random)
BATCH_TAG=$(date +%F-T%H%M%S)-R${RANDOM}
# BATCH_TAG=v0


INPUT_DATASETS=(
    mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG1.e6337_s3126_r9364_p3654
)

######################################################
# Part 2: variables you probably don't have to change
######################################################
#
# Build a zip of the files we're going to submit
ZIP=job.tgz
#
# This is the subdirectory we submit from
SUBMIT_DIR=submit
#
# This is where all the source files are
BASE=$(pwd)/training-dataset-dumper

###################################################
# Part 3: prep the submit area
###################################################
#
echo "preping submit area"
if [[ -d ${SUBMIT_DIR} ]]; then
    echo "removing old submit directory"
    rm -rf ${SUBMIT_DIR}
fi
mkdir ${SUBMIT_DIR}

##########################################
# Part 4: build a tarball of the job
###########################################
#
# Check to make sure you've properly set up the environemnt: if you
# haven't sourced the setup script in the build directory the grid
# submission will fail, so we check here before doing any work.
if ! type dump-single-btag &> /dev/null ; then
    echo "You haven't sourced x86*/setup.sh, job will fail!" >&2
    echo "quitting..." >&2
    exit 1
fi
#
echo "making tarball of local files: ${ZIP}" >&2
#
# The --outTarBall, --noSubmit, and --useAthenaPackages arguments are
# important. The --outDS and --exec don't matter at all here, they are
# just placeholders to keep panda from complianing.
prun --outTarBall=${ZIP} --noSubmit --useAthenaPackages\
     --exec "ls"\
     --outDS user.${GRID_NAME}.x

##########################################
# Part 5: loop over datasets and submit
##########################################

# Loop over all inputs
echo "submitting for ${#INPUT_DATASETS[*]} datasets"
#
for DS in ${INPUT_DATASETS[*]}
do
   # This regex extracts the DSID from the input dataset name, so
   # that we can give the output dataset a unique name. It's not
   # pretty: ideally we'd just suffix our input dataset name with
   # another tag. But thanks to insanely long job options names we
   # use in the generation stage we're running out of space for
   # everything else.
   DSID=$(sed -r 's/[^\.]*\.([0-9]{6,8})\..*/\1/' <<< ${DS})
   #
   # Build the full output dataset name
   OUT_DS=user.${GRID_NAME}.${DSID}.btagTraining.${BATCH_TAG}
   #
   # Now submit. The script we're running expects one argument per
   # input dataset, whereas %IN gives us comma separated files, so we
   # have to run it through `tr`.
   #
   echo "Submitting for ${GRID_NAME} on ${DS} -> ${OUT_DS}"
   prun --exec 'dump-single-btag $(echo %IN | tr "," " ")'\
        --outDS ${OUT_DS} --inDS ${DS}\
        --useAthenaPackages --inTarBall=${ZIP}\
        --outputs output.h5\
        --noEmail > ${OUT_DS}.log 2>&1 &
   sleep 1

done
wait
